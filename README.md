# Uncontrolled Illegal Mining and Garimpo in the Brazilian Amazon

## Authors and Affiliation
<p>Luiz Cortinhas Ferreira Neto¹,², Cesar Guerreiro Diniz¹,³,⁴, Raian Vargas Maretto⁵, Claudio Persello⁵, Maria Luize Silva Pinheiro¹​, Marcia C. Castro⁴, Luis Waldyr Rodrigues Sadeck¹,⁶​, Alexandre Fernandes Filho⁷, Julia Cansado¹, Arlesson Antonio de Almeida Souza¹,⁶, Jeremias Pinto Feitosa⁶ , Diogo Corrêa Santos³,⁸, Marcos Adami⁷, Pedro Walfir M. Souza-Filho³,⁸, Alfred Stein⁵, Andre Biehl⁹ and Aldebaro Klautau²</p>

<p>¹ Solved - Solutions in Geoinformation, Belem, PA, Brazil. ² Federal University of Para, UFPA, Technology Institute, PPGEE, Belem, PA, Brazil. ³Federal University of Para, UFPA, Geoscience Institute, PPGG, Belem, PA, Brazil. ,⁴Harvard TH Chan School of Public Health, Harvard, Boston, MA, USA. ⁵ University of Twente, UT, Department of Earth Observation Science, Faculty of Geo-information Science and Earth Observation (ITC), University of Twente, the Netherlands. ⁶National Institute for Space Research, INPE, Amazon Regional Center, Belém, PA, Brazil. ⁷National Institute for Space Research, INPE, São José do Campos, SP, Brazil. ⁸Vale Research Institute, ITV, Belem, PA, Brazil. ⁹Princeton University, Department of Computer Science, Princeton, NJ, USA.</p>

## Minimal Requisites
 - Python Language
 - Google Earth Engine Account [GMAIL]
 - Internet Connection


## Getting Started
<p> This repository shares the backbone structure of a deep-learning algorithm designed to identify mining sites in Brazil. The code produced here is a private development <b>Solved - Solutions in Geoinformations (<a src="http://www.solved.eco.br">www.solved.eco.br</a>)</b>, co-created by  <b>Luiz Cortinhas</b> (<a src="mailto:luiz.cortinhas@solved.eco.br">luiz.cortinhas@solved.eco.br | luizcf14@gmail.com</a>), <b>Julia Cansado</b> (<a src="mailto:julia.cansando@solved.eco.br"> julia.cansando@solved.eco.br </a>), <b>Maria Luize</b> (<a src="mailto:maria.luize@solved.eco.br">maria.luize@solved.eco.br</a>), and <b>Cesar Diniz</b> (<a src="mailto:cesar.diniz@solved.eco.br">cesar.diniz@solved.eco.br</a>).</p>

<p> Traditionally, most remote sensing studies are based on a deterministic relationship between the amount of reflected, emitted, or scattered electromagnetic energy and the biochemical and physical characteristics of the target being investigated. Sometimes, however, neither spectral nor temporal properties are sufficient to discriminate "super similar" targets, which may behave similarly in both the spectrum and temporal domains; this is the case within the distinction of mining patterns. Like other land uses or land cover types, this extractive pattern is intrinsically linked to soil exposure, once mining is almost always preceded by forest removal and soil excavation. CNN-based deep-learning classifiers access the context/spatial domain and unlock the capability to dissociate mining sites from other similar targets in Amazon.</p>

<p>
<b>ABSTRACT</b>

Mining has played an important role in the economies of South American countries. Although industrial mining prevails in most countries, the expansion of garimpo activity has increased substantially. Recently, Brazil exhibited two moments of garimpo dominance over industrial mining: 1989–1997 and 2019–2022. While industrial mining sites occupied ~360 km² in 1985 but increased to 1,800 km² in 2022, a 5-fold increase, garimpo mining area increased by ~1,200%, from ~218 km² in 1985 to ~2,627 km² in 2022. More than 91% of this activity is concentrated in the Amazon. Where almost 40% of the sites are five years old or younger, this proportion increases to 62% within Indigenous lands (ILs). Regarding the legal aspect, at least 77% of the 2022 extraction sites showed explicit signs of illegality. Particular attention must be given to the Kayapo, Munduruku, and Yanomami ILs. Together, they concentrate over 90% of the garimpo across ILs.
</p>
<p><b>Index Terms— Mining Activity, Amazon, CNN, Landsat, Artisanal, Industrial, Garimpo.</b></p>


## JUPYTER CODE
- [ ]  Load GEE high volume endpoint
- [ ]  Load Landsat training features
- [ ]  Load Landsat test features
- [ ]  Load empty mUNET
- [ ]  Train the model
- [ ]  Export TfRecords from GEE
- [ ]  Predict !
- [ ]  Upload to GEE
